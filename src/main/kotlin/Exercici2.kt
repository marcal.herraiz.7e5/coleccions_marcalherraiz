import java.util.Scanner
data class Empleat(val dni: Int, val nom: String, val cognoms: String, val adreca: String)

fun main() {
    val sc = Scanner(System.`in`)
    val mapOfEmpleats = mutableMapOf<Int, Empleat>()

    val nEmpleats = sc.nextInt()

    for (i in 1..nEmpleats){
        println("Dni:")
        val dni = sc.nextInt()
        println("Nom:")
        val nom = sc.next()
        println("Cognoms:")
        val cognoms = sc.next()
        println("Adreça:")
        val adreca = sc.next()

        mapOfEmpleats[dni] = Empleat(dni, nom, cognoms, adreca)
    }

    val llistaConsultes = mutableListOf<Empleat>()
    do {
        val dniEmpleat = sc.next()
        if (dniEmpleat!="END"){
            if (mapOfEmpleats.containsKey(dniEmpleat.toInt())){
                llistaConsultes.add(mapOfEmpleats.getValue(dniEmpleat.toInt()))
            }

        }
    }while (dniEmpleat!= "END")

    for (consulta in llistaConsultes){
        println("${consulta.nom} ${consulta.cognoms} - ${consulta.dni}, ${consulta.adreca}")
    }
}