import java.util.Scanner

fun main() {
     val sc = Scanner(System.`in`)
     val setOfParaules = mutableSetOf<String>()

    while (true){
        val paraula = sc.next()
        if (paraula !="END"){
            if (setOfParaules.contains(paraula)){
                println("MEEEC!")
            }else{
                setOfParaules.add(paraula)
            }
        }else{
            break
        }
    }
}