import java.util.Scanner

interface GymControlReader {
    fun nextId(): String
}

class GymControlManualReader(private val sc: Scanner = Scanner(System.`in`)) : GymControlReader {
    override fun nextId(): String = sc.next()
}

fun main() {
    val lectura: GymControlReader = GymControlManualReader()
    val usuari = mutableMapOf<String, Boolean?>()

    for (i in 1..8) {
        val id = lectura.nextId()
        if (usuari[id] == null) {
            println("$id Entrada")
            usuari[id] = true
        } else {
            println("$id Sortida")
            usuari[id] = null
        }
    }
}



