import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)
    val mapOfCartells = mutableMapOf<Int, String>()

    val nCartells = sc.nextInt()

    for (i in 1..nCartells){
        val metreCartell = sc.nextInt()
        val textCartell = sc.next()

        mapOfCartells[metreCartell] = textCartell
    }

    val nConsultes = sc.nextInt()

    val llistaConsultes = mutableListOf<String>()

    for (i in 1..nConsultes){
        val metreCartell = sc.nextInt()
        if (mapOfCartells.containsKey(metreCartell)){
            llistaConsultes.add(mapOfCartells.getValue(metreCartell))
        }else{
            llistaConsultes.add("no hi ha cartell")
        }
    }

    for (consulta in llistaConsultes){
        println(consulta)
    }
}
