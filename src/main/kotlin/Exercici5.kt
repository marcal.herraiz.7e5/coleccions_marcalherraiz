import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)
    val setOfNumeros = mutableSetOf<Int>()


    repeat(10){
        val numerosTargeta = sc.nextInt()
        setOfNumeros.add(numerosTargeta)
    }

    var numerosRestants = 10

    for (i in 1..99){
        val numeroCantat = sc.nextInt()
        if (setOfNumeros.contains(numeroCantat)){
            numerosRestants--
            if (numerosRestants==0){
                break
            }
        }
        println("Em queden $numerosRestants números")
    }
    println("BINGO")
}
