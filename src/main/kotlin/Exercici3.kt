import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)
    val mapOfVots = mutableMapOf<String, Int>()

    while (true) {
        val vot = sc.next()
        if (vot != "END") {
            mapOfVots[vot] = (mapOfVots[vot]?: 0) + 1
        } else {
            break
        }
    }

    for ((vot, suma) in mapOfVots) {
        println("$vot: $suma")
    }
}

